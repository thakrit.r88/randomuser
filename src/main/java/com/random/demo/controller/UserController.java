package com.random.demo.controller;

import com.random.demo.model.User;
import com.random.demo.model.UserResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserController {
    private final RestTemplate restTemplate;

    public UserController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
        return String.format("Hello %s!",name);
    }

    @GetMapping("/v1/users/{seed}")
    public User getRandomUser(@PathVariable String seed) {
        String apiUrl = "https://randomuser.me/api/?seed=" + seed;
        return restTemplate.getForObject(apiUrl, UserResponse.class).getResults().get(0);
    }
}