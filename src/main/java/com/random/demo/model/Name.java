package com.random.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Name {
    private String title;
    private String first;
    private String last;

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("first")
    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    @JsonProperty("last")
    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }
}
